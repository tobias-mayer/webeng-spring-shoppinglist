package com.tim19.weblab.controllers;

import com.tim19.weblab.exceptions.NotFoundException;
import com.tim19.weblab.exceptions.TextTooShortException;
import com.tim19.weblab.interfaces.ShoppingListItemService;
import com.tim19.weblab.models.ShoppingListItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/shoppinglistitems")
public class ShoppingListController {


    @Autowired
    private ShoppingListItemService shoppingListItemService;


    @GetMapping
    public List<ShoppingListItem> getAll() {
        return shoppingListItemService.getAll();
    }

    @GetMapping(value = "{id}")
    public ShoppingListItem getById(@PathVariable("id") Long id) throws NotFoundException {
        return shoppingListItemService.getById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody ShoppingListItem shoppingListItem) throws TextTooShortException {
        shoppingListItemService.create(shoppingListItem.getText(), shoppingListItem.getQuantity()).getId();
    }

    @PutMapping(value = "{id}")
    ShoppingListItem update(@PathVariable Long id, @RequestBody ShoppingListItem shoppingListItem) {
        return shoppingListItemService.update(id, shoppingListItem);
    }

    @DeleteMapping(value = "{id}")
    @ResponseStatus(HttpStatus.OK)
    void delete(@PathVariable Long id) throws NotFoundException {
        shoppingListItemService.delete(id);
    }

}
