package com.tim19.weblab.exceptions;

public class TextTooShortException extends Exception {

    public TextTooShortException(String message) {
        super(message);
    }

}
