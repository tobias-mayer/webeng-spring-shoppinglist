package com.tim19.weblab.interfaces;

import com.tim19.weblab.exceptions.NotFoundException;
import com.tim19.weblab.exceptions.TextTooShortException;
import com.tim19.weblab.models.ShoppingListItem;

import java.util.List;

public interface ShoppingListItemService {

    ShoppingListItem getById(Long id) throws NotFoundException;

    List<ShoppingListItem> getAll();

    ShoppingListItem create(String text, int quantity) throws TextTooShortException;

    ShoppingListItem update(Long id, ShoppingListItem shoppingListItem);

    void delete(Long id) throws NotFoundException;

}
