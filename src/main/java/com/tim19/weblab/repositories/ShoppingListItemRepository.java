package com.tim19.weblab.repositories;

import com.tim19.weblab.models.ShoppingListItem;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ShoppingListItemRepository extends CrudRepository<ShoppingListItem, Long> {

    ShoppingListItem findById(long id);

    List<ShoppingListItem> findAll();

}
