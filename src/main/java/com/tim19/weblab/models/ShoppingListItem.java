package com.tim19.weblab.models;

import javax.persistence.*;

@Entity
@Table(name = "shopping_list_items")
public class ShoppingListItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String text;

    private int quantity;

    protected ShoppingListItem() {}

    public ShoppingListItem(String text, int quantity) {
        this.text = text;
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return String.format("ShoppingListItem[id=%d, text='%s', quantity='%d']", id, text, quantity);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

}
