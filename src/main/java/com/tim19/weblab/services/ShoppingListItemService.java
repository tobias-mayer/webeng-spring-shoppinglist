package com.tim19.weblab.services;

import com.tim19.weblab.exceptions.NotFoundException;
import com.tim19.weblab.exceptions.TextTooShortException;
import com.tim19.weblab.models.ShoppingListItem;
import com.tim19.weblab.repositories.ShoppingListItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ShoppingListItemService implements com.tim19.weblab.interfaces.ShoppingListItemService {

    @Autowired
    private ShoppingListItemRepository shoppingListItemRepository;

    @Override
    public ShoppingListItem getById(Long id) throws NotFoundException {
        return findShoppingListItemIfExists(id);
    }

    @Override
    public List<ShoppingListItem> getAll() {
        return shoppingListItemRepository.findAll();
    }

    @Override
    public ShoppingListItem create(String text, int quantity) throws TextTooShortException {
        if (text.length() > 0) {
            ShoppingListItem item = new ShoppingListItem(text, quantity);
            return shoppingListItemRepository.save(item);
        }
        throw new TextTooShortException("Text is to short");
    }

    @Override
    public ShoppingListItem update(Long id, ShoppingListItem shoppingListItem) {
        shoppingListItem.setId(id);
        return shoppingListItemRepository.save(shoppingListItem);
    }

    @Override
    public void delete(Long id) throws NotFoundException {
        ShoppingListItem shoppingListItem = findShoppingListItemIfExists(id);
        shoppingListItemRepository.delete(shoppingListItem);
    }

    private ShoppingListItem findShoppingListItemIfExists(Long id) throws NotFoundException {
        Optional<ShoppingListItem> item = shoppingListItemRepository.findById(id);
        if (item.isPresent()) {
            return item.get();
        }
        throw new NotFoundException(String.format("No Item found with the id %d", id));
    }

}
