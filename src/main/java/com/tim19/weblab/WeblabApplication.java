package com.tim19.weblab;

import com.tim19.weblab.models.ShoppingListItem;
import com.tim19.weblab.repositories.ShoppingListItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;

@SpringBootApplication
public class WeblabApplication {

	private static final Logger logger = LoggerFactory.getLogger(WeblabApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(WeblabApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(ShoppingListItemRepository repository) {
		return (args -> {
			ShoppingListItem item1 = new ShoppingListItem("Milch", 2);
			ShoppingListItem item2 = new ShoppingListItem("Eier", 1);
			ShoppingListItem item3 = new ShoppingListItem("Käse", 8);
			ShoppingListItem item4 = new ShoppingListItem("Kuchen", 3);

			repository.save(item1);
			repository.save(item2);
			repository.save(item3);
			repository.save(item4);

			List<ShoppingListItem> allItems = repository.findAll();
			for (ShoppingListItem item : allItems) {
				logger.info(item.toString());
			}

			logger.info("Finding by id '2'");
			ShoppingListItem secondItem = repository.findById(2);
			logger.info(secondItem.toString());

		});
	}

}
